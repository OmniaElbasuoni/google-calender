package com.example.eventsapp.adapter;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventsapp.CalendarActivity;
import com.example.eventsapp.R;
import com.example.eventsapp.model.ScheduledEvents;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;

import java.io.IOException;
import java.util.List;

/**
 * Created by Omnia on 14-Sep-19.
 */

public class EventListAdapter extends BaseAdapter {
    private CalendarActivity context;
    private List<ScheduledEvents> scheduledEvents;
    private LayoutInflater inflater;

   public EventListAdapter(CalendarActivity context, List<ScheduledEvents> scheduledEvents){
        this.context = context;
        this.scheduledEvents = scheduledEvents;
        inflater = LayoutInflater.from(this.context);
    }




    @Override
    public int getCount() {
        return scheduledEvents.size();
    }

    @Override
    public Object getItem(int i) {
        return scheduledEvents.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EventHolder eventHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.event_view_layout, parent, false);
            eventHolder = new EventHolder(convertView);
            convertView.setTag(eventHolder);
        } else {
            eventHolder = (EventHolder) convertView.getTag();
        }
        final ScheduledEvents scheduledEvents = (ScheduledEvents) getItem(position);


        if (scheduledEvents.getWeatherCondition() != null)
        {
            eventHolder.eventWeather.setText(scheduledEvents.getWeatherCondition());
        }

        else {
            eventHolder.eventTitle.setText("No Weather Condition For This Event !");
        }


        if (scheduledEvents.getEventSummery() != null)
        {
            eventHolder.eventTitle.setText(scheduledEvents.getEventSummery());
        }
        else {
            eventHolder.eventTitle.setText("No Summery Added For This Event !");
        }

        if (scheduledEvents.getDescription() != null) {
            eventHolder.eventDes.setText(scheduledEvents.getDescription());

        }

        else {
            eventHolder.eventDes.setText("No Description For This Event !");
        }
/*
        eventHolder.eventAttendee.setText(scheduledEvents.getAttendees());
*/


if (scheduledEvents.getStartDate() != null)

{
    eventHolder.eventStart.setText(scheduledEvents.getStartDate());
}

else {
    eventHolder.eventStart.setText("No Start Date For This Event !");

    }


if (scheduledEvents.getEndDate() !=null) {

    eventHolder.eventEnd.setText(scheduledEvents.getEndDate());
}

else {
    eventHolder.eventEnd.setText("No End Date For This Event !");

}

if (scheduledEvents.getLocation() != null)
{
    eventHolder.eventLocation.setText(scheduledEvents.getLocation());

}
else {
    eventHolder.eventLocation.setText("No Location For This Event !");

}

if (scheduledEvents.getStatus() != null) {


    eventHolder.eventStatus.setText(scheduledEvents.getStatus());
}
else {
    eventHolder.eventStatus.setText("No Status For This Event !");

}

/*if (scheduledEvents.getDate() != null) {

    eventHolder.eventDate.setText(String.valueOf(scheduledEvents.getDate()));
}
else {
    eventHolder.eventDate.setText("No Date For This Event !");

}*/

        eventHolder.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateTask(context.mCredential, scheduledEvents.getEventId(), "accepted").execute();
            }
        });

        eventHolder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateTask(context.mCredential, scheduledEvents.getEventId(), "declined").execute();
            }
        });



        return convertView;
    }
    private class EventHolder {
        TextView eventTitle, eventDes,eventDate, eventStatus, eventStart, eventEnd, eventLocation,eventWeather;
        Button confirm,decline;

        public EventHolder(View item) {
            eventTitle =  item.findViewById(R.id.eventname);
            eventDes = item.findViewById(R.id.eventdescription);
            eventDate=item.findViewById(R.id.eventdate);
            eventWeather=item.findViewById(R.id.weatherState);

            eventStatus =  item.findViewById(R.id.eventStatus);

            eventStart =  item.findViewById(R.id.eventStartTime);
            eventEnd =  item.findViewById(R.id.eventEndTime);
            eventLocation = item.findViewById(R.id.eventLocation);
            confirm=item.findViewById(R.id.acceptevent);
            decline=item.findViewById(R.id.declineevent);
        }
    }

    public class UpdateTask extends AsyncTask<Void, Void, List<String>> {

        private Exception mLastError = null;
        private boolean FLAG = false;
      public   String eventID,status;

        public UpdateTask(GoogleAccountCredential credential, String eventID, String status) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            context.mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
            this.eventID=eventID;
            this.status=status;
        }

        /**
         * Background task to call Google Calendar API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                getDataFromApi();
            } catch (Exception e) {
                e.printStackTrace();
                mLastError = e;
                cancel(true);
                return null;
            }
            return null;
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         *
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private void getDataFromApi() throws IOException {
            Event currentEvent= context.mService.events().get("primary", eventID).execute();
            if (currentEvent.getAttendees() != null) {
                for (EventAttendee attendee : currentEvent.getAttendees()) {
                    if (attendee.getSelf() !=null && attendee.getSelf()) {
                        attendee.setResponseStatus(status);
                        break;
                    }
                }
            }
            context.mService.events().update("primary", eventID, currentEvent).execute();
        }

        @Override
        protected void onPreExecute() {
            context.mOutputText.setText("");
            context.mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            context.mProgress.hide();
            System.out.println("--------------------" + scheduledEvents.size());
            notifyDataSetChanged();
        }

        @Override
        protected void onCancelled() {
            context.mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    context.showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    context.startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            CalendarActivity.REQUEST_AUTHORIZATION);
                } else {
                    context.mOutputText.setText("The following error occurred:\n"
                            + mLastError.getMessage());
                }
            } else {
                context.mOutputText.setText("Request cancelled.");
            }
        }
    }

}
