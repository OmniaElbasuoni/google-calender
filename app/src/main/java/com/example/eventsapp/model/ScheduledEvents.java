package com.example.eventsapp.model;

import com.google.api.client.util.DateTime;

/**
 * Created by Omnia on 14-Sep-19.
 */

public class ScheduledEvents {
    private String eventId;
    private String calendarId;
    private String eventSummery;

    public String getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    private String description;

    private String Status;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    private String location;
    private String WeatherCondition;

    public String getWeatherCondition() {
        return WeatherCondition;
    }

    public void setWeatherCondition(String weatherCondition) {
        WeatherCondition = weatherCondition;
    }

   /* public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }*/

/*
    private DateTime date;
*/

    private String startDate;
    private String endDate;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventSummery() {
        return eventSummery;
    }

    public void setEventSummery(String eventSummery) {
        this.eventSummery = eventSummery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

  /*  public String getAttendees() {
        return attendees;
    }

    public void setAttendees(String attendees) {
        this.attendees = attendees;
    }*/

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }


    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
